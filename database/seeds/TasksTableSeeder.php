<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
            [
                [
                        'title' => 'Test Task1',
                        'user_id' => '1',
                        'status' => '0',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'title' => 'Test Task2',
                    'user_id' => '1',
                    'status' => '0',
                    'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Test Task3',
                'user_id' => '1',
                'status' => '0',
                'created_at' => date('Y-m-d G:i:s'),
            ],
                    ]);
    }

}


