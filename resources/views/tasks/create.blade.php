@extends('layouts.app')
@section('content')


<h1>Create a New Task</h1>
<form method = 'post' action="{{action('TaskController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">What Task Would you Like to Add?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add">
</div>

</form>
@endsection