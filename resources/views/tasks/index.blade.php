
@extends('layouts.app')
@section('content')

<h1>This the Tasks on Dashbored</h1>
<a href="{{route('mytasks',$id)}}">My Tasks</a>

<ul>
    @foreach($tasks as $task)
    <li>
       id: {{$task->id}}  
       title:  {{$task->title}} <a href= "{{route('tasks.edit', $task->id )}}">Edit</a>
       @cannot('user')<a href= "{{route('delete', $task->id )}}"> Delete</a>@endcannot 
       @if($task->status == 1)
       <a>  Done</a>
       @else
       @cannot('user') <a href= "{{route('statusupdate', $task->id )}}">Mark as Done </a>@endcannot 
    </li>
    @endif
    @endforeach
</ul>

<a href="{{route('tasks.create')}}">Create a New Task </a>
@endsection