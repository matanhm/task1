<?php
namespace App\Http\Controllers;
use App\Task; 
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
       $tasks = Task::all();
       return view('tasks.index',['tasks'=>$tasks],['id'=>$id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
          abort(403,"Are you a hacker or what?");
          }
       return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->user_id = $id;
        $task->save();
        return redirect('tasks');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
       $task = Task::find($id);
       return view('tasks.edit', compact('task'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
          $task = Task::find($id);
          $task -> update($request->all());
          return redirect('tasks');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
       $task = Task::find($id);
       $task->delete();
       return redirect('tasks');

    }

    public function update_status($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $task = Task::find($id);
        $task->status = 1;
        $task -> update();
        return redirect('tasks');
    }

    public function mytasks()
    {
        $id = Auth::id();
        $tasks = User::find($id)->tasks;
        return view('tasks.mytasks', ['tasks' => $tasks]);

    }
}
